package com.atlassian.webhooks.plugin.event;

public class EventForFilterTest
{
    private final String value;

    public EventForFilterTest(final String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }
}
