package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.webhooks.spi.provider.EventMatcher;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;

public final class LegacyEventWithPluginAndModuleKey
{
    public final String pluginKey;
    public final String moduleKey;

    public LegacyEventWithPluginAndModuleKey(final String pluginKey, final String moduleKey)
    {
        this.pluginKey = pluginKey;
        this.moduleKey = moduleKey;
    }

    public static EventMatcher<LegacyEventWithPluginAndModuleKey> MATCHER = new EventMatcher<LegacyEventWithPluginAndModuleKey>() {
        @Override
        public boolean matches(final LegacyEventWithPluginAndModuleKey event, final Object listenerParameters)
        {
            if (listenerParameters instanceof PluginModuleListenerParameters)
            {
                PluginModuleListenerParameters params = (PluginModuleListenerParameters) listenerParameters;
                return (event.pluginKey + event.moduleKey).equals(params.getPluginKey() + params.getModuleKey().orNull());
            }
            return false;
        }
    };
}
