package com.atlassian.webhooks.plugin.test.servlet;

public final class FilteredEventsServlet extends AbstractWebHookListeningServlet
{
    public static final String QUEUE_NAME = "FilteredEventsQueue";

    protected FilteredEventsServlet(final WebHooksQueues webHooksQueues)
    {
        super(webHooksQueues);
    }

    @Override
    String queueName()
    {
        return QUEUE_NAME;
    }
}
