package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.webhooks.spi.provider.EventMatcher;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;

public final class LegacyMatcherEvent
{
    private final boolean shouldMatch;

    public LegacyMatcherEvent(final boolean shouldMatch)
    {
        this.shouldMatch = shouldMatch;
    }

    public boolean isShouldMatch()
    {
        return shouldMatch;
    }

    public static final EventMatcher<LegacyMatcherEvent> MATCHER = new EventMatcher<LegacyMatcherEvent>()
    {
        @Override
        public boolean matches(final LegacyMatcherEvent event, final Object listenerParameters)
        {
            if (listenerParameters instanceof PluginModuleListenerParameters)
            {
                boolean shouldMatch = (Boolean) ((PluginModuleListenerParameters) listenerParameters).getParams().get("shouldMatch");
                return event.isShouldMatch() == shouldMatch;
            }
            return false;
        }
    };
}
