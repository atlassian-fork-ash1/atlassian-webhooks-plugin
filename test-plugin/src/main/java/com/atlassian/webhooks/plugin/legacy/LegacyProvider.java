package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.webhooks.spi.provider.EventMatcher;
import com.atlassian.webhooks.spi.provider.EventSerializationException;
import com.atlassian.webhooks.spi.provider.EventSerializer;
import com.atlassian.webhooks.spi.provider.EventSerializerFactory;
import com.atlassian.webhooks.spi.provider.EventSerializers;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;
import com.atlassian.webhooks.spi.provider.WebHookProvider;
import com.atlassian.webhooks.spi.provider.WebHookRegistrar;
import com.google.common.collect.ImmutableMap;

/**
 * This is a provider which supplies webhook events using a legacy provider.
 */
public class LegacyProvider implements WebHookProvider
{
    @Override
    public void provide(final WebHookRegistrar registrar)
    {
        registrar.webhook("legacy_provider_event")
                .whenFired(LegacyProviderEvent.class)
                .matchedBy(new EventMatcher<LegacyProviderEvent>()
                {
                    @Override
                    public boolean matches(final LegacyProviderEvent event, final Object listenerParameters)
                    {
                        return event.isShouldBeMatched();
                    }
                })
                .serializedWith(new EventSerializerFactory<LegacyProviderEvent>()
                {
                    @Override
                    public EventSerializer create(final LegacyProviderEvent event)
                    {
                        return EventSerializers.forMap(event, ImmutableMap.<String, Object>of("payload", event.getSomePayload()));
                    }
                });

        registrar.webhook("legacy_event_matcher")
                .whenFired(LegacyMatcherEvent.class)
                .matchedBy(LegacyMatcherEvent.MATCHER)
                .serializedWith(new EventSerializerFactory<LegacyMatcherEvent>() {
                    @Override
                    public EventSerializer create(final LegacyMatcherEvent event)
                    {
                        return EventSerializers.forMap(event, ImmutableMap.<String, Object>of("shouldMatch", event.isShouldMatch()));
                    }
                });

        registrar.webhook("legacy_plugin_key_event_matcher")
                .whenFired(LegacyEventWithPluginAndModuleKey.class)
                .matchedBy(LegacyEventWithPluginAndModuleKey.MATCHER)
                .serializedWith(new EventSerializerFactory<LegacyEventWithPluginAndModuleKey>() {
                    @Override
                    public EventSerializer create(final LegacyEventWithPluginAndModuleKey event)
                    {
                        return EventSerializers.forMap(event, ImmutableMap.<String, Object>of("pluginKey", event.pluginKey, "moduleKey", event.moduleKey));
                    }
                });

    }
}
