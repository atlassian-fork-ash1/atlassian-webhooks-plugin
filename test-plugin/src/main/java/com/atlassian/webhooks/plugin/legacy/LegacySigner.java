package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.httpclient.api.Request;
import com.atlassian.webhooks.spi.plugin.RequestSigner;

import java.net.URI;

public class LegacySigner implements RequestSigner
{
    @Override
    public void sign(final URI uri, final String pluginKey, final Request.Builder request)
    {
        request.setHeader("signed-with-legacy-signer", "true");
        request.setHeader("plugin-key", pluginKey);
    }
}
