package com.atlassian.webhooks.plugin.management;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.ErrorMessage;
import com.atlassian.webhooks.api.util.MessageCollection;
import com.atlassian.webhooks.spi.WebHookListenerActionValidator;

public class WebHookListenerActionValidatorImpl implements WebHookListenerActionValidator
{
    public static final String INVALID_WEBHOOK_NAME = "Invalid name";

    @Override
    public MessageCollection validateWebHookRegistration(final PersistentWebHookListener registrationParameters)
    {
        return validateName(registrationParameters);
    }

    @Override
    public MessageCollection validateWebHookUpdate(final PersistentWebHookListener registrationParameters)
    {
        return validateName(registrationParameters);
    }

    @Override
    public MessageCollection validateWebHookRemoval(final PersistentWebHookListener registrationParameters)
    {
        return validateName(registrationParameters);
    }

    private MessageCollection validateName(final PersistentWebHookListener registrationParameters)
    {
        if (registrationParameters.getName().equals(INVALID_WEBHOOK_NAME))
        {
            return MessageCollection.builder().addMessage(new ErrorMessage("name", new String[] {"Invalid webhook name"}), MessageCollection.Reason.VALIDATION_FAILED).build();
        }
        return MessageCollection.empty();
    }
}
