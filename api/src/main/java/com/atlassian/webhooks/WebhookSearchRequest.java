package com.atlassian.webhooks;

import javax.annotation.Nonnull;

import static com.atlassian.webhooks.util.PropertyUtil.getProperty;

/**
 * Request for {@link WebhookService#search searching} for registered {@link Webhook}
 *
 * @since 5.0
 */
public class WebhookSearchRequest extends AbstractBulkWebhookRequest {

    private static final int DEFAULT_LIMIT = getProperty("search.default.limit", 250);

    private final int limit;
    private final int offset;

    WebhookSearchRequest(Builder builder) {
        super(builder);
        this.limit = builder.limit;
        this.offset = builder.offset;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public static Builder builder(@Nonnull AbstractBulkWebhookRequest request) {
        return new Builder(request);
    }

    @Nonnull
    public static Builder builder(@Nonnull WebhookSearchRequest request) {
        return new Builder(request);
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    public static class Builder extends AbstractBuilder<Builder> {

        private int limit;
        private int offset;

        public Builder() {
            offset = 0;
            limit = DEFAULT_LIMIT;
        }

        public Builder(@Nonnull AbstractBulkWebhookRequest request) {
            super(request);
            offset = 0;
            limit = DEFAULT_LIMIT;
        }

        public Builder(@Nonnull WebhookSearchRequest request) {
            super(request);
            limit = request.getLimit();
            offset = request.getOffset();
        }

        @Nonnull
        public WebhookSearchRequest build() {
            return new WebhookSearchRequest(this);
        }

        @Nonnull
        public Builder limit(int value) {
            if (value <= 0) {
                throw new IllegalArgumentException("limit must be greater than zero");
            }
            limit = value;

            return self();
        }

        @Nonnull
        public Builder offset(int value) {
            if (value < 0) {
                throw new IllegalArgumentException("offset cannot be negative");
            }
            offset = value;

            return self();
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
