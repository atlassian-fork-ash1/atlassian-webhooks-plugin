package com.atlassian.webhooks.request;

import javax.annotation.Nonnull;

/**
 * A HTTP header on a webhook request or response
 *
 * @since 5.0
 */
public interface Header {

    @Nonnull
    String getName();

    @Nonnull
    String getValue();
}
