package com.atlassian.webhooks;

import org.hibernate.validator.constraints.URL;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;

/**
 * A request to {@link WebhookService#ping send} a ping message to a webhook listener
 *
 * @since 5.0
 */
public class PingRequest {

    private final String url;
    private final WebhookScope scope;

    private PingRequest(Builder builder) {
        url = builder.url;
        scope = builder.scope;
    }

    @Nonnull
    public static Builder builder(@Nonnull String url) {
        return new Builder(url);
    }

    @Nonnull
    public static Builder builder(@Nonnull Webhook webhook) {
        return new Builder(webhook.getUrl())
                .scope(webhook.getScope());
    }

    @NotNull
    @URL
    public String getUrl() {
        return url;
    }

    @NotNull
    public WebhookScope getScope() {
        return scope;
    }

    public static class Builder {

        private final String url;
        private WebhookScope scope;

        public Builder(@Nonnull String url) {
            this.url = url;
            this.scope = WebhookScope.GLOBAL;
        }

        @Nonnull
        public Builder scope(@Nonnull WebhookScope scope) {
            this.scope = scope;
            return this;
        }

        @Nonnull
        public PingRequest build() {
            return new PingRequest(this);
        }
    }
}
