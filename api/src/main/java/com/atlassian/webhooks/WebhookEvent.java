package com.atlassian.webhooks;

import javax.annotation.Nonnull;

public interface WebhookEvent {

    @Nonnull
    @WebhookEventId
    String getId();

    @Nonnull
    String getI18nKey();
}
