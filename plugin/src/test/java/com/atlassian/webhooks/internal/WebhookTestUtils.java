package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookConfigurationEntry;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookEvent;
import com.atlassian.webhooks.internal.model.SimpleWebhook;
import com.atlassian.webhooks.internal.model.SimpleWebhookScope;
import com.google.common.collect.ImmutableList;

import java.util.*;
import java.util.stream.Collectors;

import static com.atlassian.webhooks.internal.TestEvent.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WebhookTestUtils {

    public static final String DEFAULT_URL = "http://something.com";

    private static final String ACTIVE = "ACTIVE";
    private static final List<WebhookEvent> DEFAULT_EVENTS = ImmutableList.of(PROJ_CREATE, PROJ_DELETE, PROJ_UPDATE, USER_CREATE);
    private static final String DEFAULT_NAME = "Hook name";
    private static final WebhookScope DEFAULT_SCOPE = new SimpleWebhookScope("DEFAULT_TYPE", "scope-id");
    private static final Date UPDATED_DATE = new Date();

    private static Map<String, String> defaultContext = null;

    public static void assertStandardWebhook(AoWebhook aoWebhook) {
        assertStandardWebhook(convert(aoWebhook));
    }

    public static void assertStandardWebhook(Webhook webhook) {

        assertThat(webhook.getName(), equalTo(DEFAULT_NAME));
        assertThat(webhook.getUrl(), equalTo(DEFAULT_URL));

        assertThat(webhook.getEvents(), containsInAnyOrder(DEFAULT_EVENTS.toArray()));
        assertThat(webhook.getEvents(), hasSize(DEFAULT_EVENTS.size()));

        WebhookScope scope = webhook.getScope();
        assertThat(scope, notNullValue());
        assertThat(scope.getType(), equalTo("DEFAULT_TYPE"));
        assertThat(scope.getId().orElse(null), equalTo("scope-id"));

        assertThat(webhook.getConfiguration().entrySet(), hasSize(defaultContext.size()));
        assertThat(webhook.getConfiguration().entrySet(), everyItem(isIn(defaultContext.entrySet())));
        // the dates are gte UPDATED_DATE because AoWebhookDao.create/update sets the date to the current date,
        // which is guaranteed to be gte UPDATED_DATE
        assertThat(webhook.getCreatedDate(), greaterThanOrEqualTo(UPDATED_DATE));
        assertThat(webhook.getUpdatedDate(), greaterThanOrEqualTo(UPDATED_DATE));
    }

    public static AoWebhook mockStandardAoWebhook() {
        AoWebhook webhook = mock(AoWebhook.class);

        AoWebhookEvent[] events = new AoWebhookEvent[DEFAULT_EVENTS.size()];
        for (int i = 0; i < DEFAULT_EVENTS.size(); i++) {
            events[i] = mock(AoWebhookEvent.class);
            when(events[i].getEventId()).thenReturn(DEFAULT_EVENTS.get(i).getId());
            when(events[i].getWebhook()).thenReturn(webhook);
        }

        AoWebhookConfigurationEntry[] context = getConfiguration().entrySet().stream().map(entry -> {
            AoWebhookConfigurationEntry contextEntry = mock(AoWebhookConfigurationEntry.class);
            when(contextEntry.getKey()).thenReturn(entry.getKey());
            when(contextEntry.getValue()).thenReturn(entry.getValue());
            when(contextEntry.getWebhook()).thenReturn(webhook);
            return contextEntry;
        }).toArray(AoWebhookConfigurationEntry[]::new);

        when(webhook.getCreatedDate()).thenReturn(UPDATED_DATE);
        when(webhook.getConfiguration()).thenReturn(context);
        when(webhook.getName()).thenReturn(DEFAULT_NAME);
        when(webhook.getEvents()).thenReturn(events);
        when(webhook.getScopeId()).thenReturn(DEFAULT_SCOPE.getId().orElse(null));
        when(webhook.getScopeType()).thenReturn(DEFAULT_SCOPE.getType());
        when(webhook.getUrl()).thenReturn(DEFAULT_URL);
        when(webhook.getUpdatedDate()).thenReturn(UPDATED_DATE);
        return webhook;
    }

    public static WebhookCreateRequest standardWebhookRequest() {
        return WebhookCreateRequest.builder()
                .configuration(getConfiguration())
                .event(DEFAULT_EVENTS)
                .name(DEFAULT_NAME)
                .scope(DEFAULT_SCOPE)
                .url(DEFAULT_URL)
                .build();
    }

    private static Webhook convert(AoWebhook aoWebhook) {
        if (aoWebhook == null) {
            return null;
        }

        Map<String, String> context = new HashMap<>();
        Arrays.stream(aoWebhook.getConfiguration())
                .forEach(ctx -> context.put(ctx.getKey(), ctx.getValue()));

        boolean active = context.containsKey(ACTIVE) && Boolean.valueOf(context.get(ACTIVE));

        return SimpleWebhook.builder()
                .id(aoWebhook.getID())
                .active(active)
                .configuration(context)
                .name(aoWebhook.getName())
                .scope(getScope(aoWebhook))
                .event(getEvents(aoWebhook.getEvents()))
                .url(aoWebhook.getUrl())
                .build();
    }

    private static Map<String, String> getConfiguration() {
        if (defaultContext != null) return defaultContext;

        defaultContext = new HashMap<>();
        defaultContext.put("hi there", "all");
        defaultContext.put("no value", "value");
        return defaultContext;
    }

    private static WebhookScope getScope(AoWebhook webhook) {
        return new SimpleWebhookScope(webhook.getScopeType(), webhook.getScopeId());
    }

    private static List<WebhookEvent> getEvents(AoWebhookEvent[] events) {
        return Arrays.stream(events)
                .map(AoWebhookEvent::getEventId)
                .map(TestEvent::fromId)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
