package com.atlassian.webhooks.internal.history;

import com.atlassian.webhooks.history.DetailedInvocationError;
import com.atlassian.webhooks.history.InvocationOutcome;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.io.PrintWriter;
import java.io.StringWriter;

import static com.atlassian.webhooks.history.InvocationOutcome.ERROR;
import static java.util.Objects.requireNonNull;

public class SimpleDetailedError implements DetailedInvocationError {

    protected static final int MAX_PAYLOAD_SIZE_CHARS = 1024 * 8;

    private final String content;
    private final String description;

    public SimpleDetailedError(@Nonnull Throwable error) {
        content = extractStackTrace(error);
        description = describe(error);
    }

    public SimpleDetailedError(@Nonnull String content, @Nonnull String description) {
        this.content = requireNonNull(content, "content");
        this.description = requireNonNull(description, "description");
    }

    @Override
    @Nonnull
    public String getContent() {
        return content;
    }

    @Nonnull
    @Override
    public InvocationOutcome getOutcome() {
        return ERROR;
    }

    @Nonnull
    @Override
    public String getDescription() {
        return description;
    }

    private String describe(Throwable error) {
        String message = StringUtils.defaultString(error.getMessage());
        return error.getClass().getName() + (message.isEmpty() ? "" : (": " + message));
    }

    private String extractStackTrace(@Nonnull Throwable error) {
        StringWriter stackTraceAsString = new StringWriter(1024);
        error.printStackTrace(new PrintWriter(stackTraceAsString));
        String stackTrace = stackTraceAsString.getBuffer().toString();
        return StringUtils.truncate(stackTrace, MAX_PAYLOAD_SIZE_CHARS);
    }
}
