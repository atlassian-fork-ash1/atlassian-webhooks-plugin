package com.atlassian.webhooks.internal;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookPayloadBuilder;
import com.atlassian.webhooks.WebhookPayloadProvider;
import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;

@Component
@ExportAsService({WebhookPayloadProvider.class})
public class DiagnosticsPayloadProvider implements WebhookPayloadProvider {

    @Override
    public int getWeight() {
        return 100000; // very low priority
    }

    @Override
    public void setPayload(@Nonnull WebhookInvocation invocation, @Nonnull WebhookPayloadBuilder builder) {
        builder.body("{\"test\": true}".getBytes(StandardCharsets.UTF_8), "application/json");
    }

    @Override
    public boolean supports(@Nonnull WebhookInvocation invocation) {
        return invocation.getEvent() == WebhookDiagnosticsEvent.PING;
    }
}
