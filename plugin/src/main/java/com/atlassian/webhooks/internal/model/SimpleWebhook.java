package com.atlassian.webhooks.internal.model;


import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.util.BuilderUtil;

import javax.annotation.Nonnull;
import java.util.*;

import static java.util.Objects.requireNonNull;

public class SimpleWebhook implements Webhook {

    private final boolean active;
    private final Map<String, String> configuration;
    private final Date createdDate;
    private final Set<WebhookEvent> events;
    private final int id;
    private final String name;
    private final WebhookScope scope;
    private final Date updatedDate;
    private final String url;

    private SimpleWebhook(Builder builder) {
        this.active = builder.active;
        this.configuration = builder.configuration;
        this.createdDate = requireNonNull(builder.createdDate, "updatedDate");
        this.events = builder.events;
        this.id = builder.id;
        this.name = builder.name;
        this.scope = requireNonNull(builder.scope, "scope");
        this.updatedDate = requireNonNull(builder.updatedDate, "updatedDate");
        this.url = builder.url;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @Override
    public Map<String, String> getConfiguration() {
        return configuration;
    }

    @Nonnull
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Nonnull
    @Override
    public Set<WebhookEvent> getEvents() {
        return events;
    }

    @Override
    public int getId() {
        return id;
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Nonnull
    @Override
    public WebhookScope getScope() {
        return scope;
    }

    @Nonnull
    @Override
    public Date getUpdatedDate() {
        return updatedDate;
    }

    @Nonnull
    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    public static class Builder {

        private final Map<String, String> configuration;
        private final Set<WebhookEvent> events;

        private boolean active;
        private Date createdDate;
        private int id;
        private String name;
        private WebhookScope scope;
        private Date updatedDate;
        private String url;

        private Builder() {
            active = true;
            configuration = new HashMap<>();
            createdDate = new Date();
            events = new HashSet<>();
            scope = WebhookScope.GLOBAL;
            updatedDate = createdDate;
        }

        @Nonnull
        public Webhook build() {
            return new SimpleWebhook(this);
        }

        @Nonnull
        public Builder configuration(@Nonnull Map<String, String> value) {
            configuration.putAll(requireNonNull(value, "configuration"));
            return this;
        }

        @Nonnull
        public Builder createdDate(@Nonnull Date value) {
            createdDate = requireNonNull(value, "createdDate");
            return this;
        }

        @Nonnull
        public Builder active(boolean value) {
            active = value;
            return this;
        }

        @Nonnull
        public Builder event(@Nonnull Iterable<WebhookEvent> value) {
            BuilderUtil.addIf(Objects::nonNull, events, value);
            return this;
        }

        @Nonnull
        public Builder event(@Nonnull WebhookEvent value, WebhookEvent... values) {
            BuilderUtil.addIf(Objects::nonNull, events, value, values);
            return this;
        }

        @Nonnull
        public Builder id(int value) {
            id = value;
            return this;
        }

        @Nonnull
        public Builder name(@Nonnull String value) {
            requireNonNull(value, "value");
            name = value;
            return this;
        }

        @Nonnull
        public Builder scope(@Nonnull WebhookScope value) {
            scope = requireNonNull(value, "scope");

            return this;
        }

        @Nonnull
        public Builder url(@Nonnull String value) {
            requireNonNull(value);
            this.url = value;
            return this;
        }

        @Nonnull
        public Builder updatedDate(@Nonnull Date value) {
            updatedDate = requireNonNull(value, "updatedDate");
            return this;
        }
    }
}
