package com.atlassian.webhooks.internal.client;

import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

import javax.annotation.Nonnull;
import java.util.concurrent.CompletableFuture;

public interface RequestExecutor {

    /**
     * Execute the request to the external service
     *
     * @param request details about the request
     * @return a future detailing the response
     */
    @Nonnull
    CompletableFuture<WebhookHttpResponse> execute(@Nonnull WebhookHttpRequest request);
}
