package com.atlassian.webhooks.internal.history;

import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.history.HistoricalInvocation;
import com.atlassian.webhooks.history.InvocationRequest;
import com.atlassian.webhooks.history.InvocationResult;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.time.Instant;

import static java.util.Objects.requireNonNull;

public class SimpleHistoricalInvocation implements HistoricalInvocation {

    private final WebhookEvent event;
    private final Instant finish;
    private final InvocationRequest request;
    private final InvocationResult result;
    private final Instant start;
    private final String id;

    protected SimpleHistoricalInvocation(@Nonnull String id, @Nonnull WebhookEvent event,
                                         @Nonnull Instant start, @Nonnull Instant finish,
                                         @Nonnull InvocationRequest request, @Nonnull InvocationResult result) {
        this.id = requireNonNull(id, "id");
        this.event = requireNonNull(event, "event");
        this.finish = requireNonNull(finish, "finish");
        this.request = requireNonNull(request, "request");
        this.result = requireNonNull(result, "result");
        this.start = requireNonNull(start, "start");
    }

    @Nonnull
    @Override
    public Duration getDuration() {
        return Duration.between(start, finish);
    }

    @Nonnull
    @Override
    public WebhookEvent getEvent() {
        return event;
    }

    @Nonnull
    @Override
    public Instant getFinish() {
        return finish;
    }

    @Nonnull
    @Override
    public String getId() {
        return id;
    }

    @Nonnull
    @Override
    public InvocationRequest getRequest() {
        return request;
    }

    @Nonnull
    @Override
    public InvocationResult getResult() {
        return result;
    }

    @Nonnull
    @Override
    public Instant getStart() {
        return start;
    }
}
