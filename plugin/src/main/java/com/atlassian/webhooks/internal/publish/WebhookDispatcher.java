package com.atlassian.webhooks.internal.publish;

import javax.annotation.Nonnull;

/**
 * Used to publish webhooks to their destinations
 *
 * @since 5.0
 */
public interface WebhookDispatcher {

    /**
     * Dispatch webhooks that match the associated request.
     *
     * @param invocation the webhook and invocation data to publish
     */
    void dispatch(@Nonnull InternalWebhookInvocation invocation);

    /**
     * @return the number of webhook dispatches currently in flight
     */
    int getInFlightCount();

    /**
     * @return the timestamp of the last time a dispatch was rejected because too many dispatches were already in flight
     */
    long getLastRejectedTimestamp();
}
