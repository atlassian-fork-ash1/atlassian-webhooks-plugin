package com.atlassian.webhooks.internal;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhooksConfiguration;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("webhooksLifecycle")
@ExportAsService(LifecycleAware.class)
public class WebhooksLifecycle implements LifecycleAware {

    private final WebhookHostAccessor hostAccessor;
    private final WebhookServiceRegistrar registrar;
    private final List<WebhooksLifecycleAware> services;
    private final WebhookService webhookService;

    @Autowired
    public WebhooksLifecycle(WebhookHostAccessor hostAccessor, List<WebhooksLifecycleAware> services,
                             WebhookService webhookService) {

        this.hostAccessor = hostAccessor;
        this.services = services;
        this.webhookService = webhookService;

        registrar = new WebhookServiceRegistrar();
    }

    @Override
    public void onStart() {
        registrar.register(webhookService);

        WebhooksConfiguration configuration = hostAccessor.getConfiguration().orElse(WebhooksConfiguration.DEFAULT);
        services.forEach(service -> service.onStart(configuration));
    }

    @Override
    public void onStop() {
        registrar.register(null);
        services.forEach(WebhooksLifecycleAware::onStop);
    }
}
