package com.atlassian.webhooks.internal.dao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookConfigurationEntry;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookEvent;
import com.atlassian.webhooks.internal.dao.ao.v0.WebHookListenerAOV0;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

import static com.google.common.base.Preconditions.checkState;

public class MigrateToV1Task implements ActiveObjectsUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(MigrateToV1Task.class);

    @Override
    public ModelVersion getModelVersion() {
        return ModelVersion.valueOf("1");
    }

    @Override
    public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {
        checkState(currentVersion.isSame(ModelVersion.valueOf("0")),
                "Can only migrate from v0 to v1");

        log.info("Migrating webhooks to version 1");

        ao.migrate(WebHookListenerAOV0.class, AoWebhook.class, AoWebhookConfigurationEntry.class, AoWebhookEvent.class);
        ao.stream(WebHookListenerAOV0.class, hook -> migrate(ao, hook));

        log.info("Migration is complete");
    }

    private Map<String, String> createConfiguration(WebHookListenerAOV0 webHookListenerAOV0) {
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        if (StringUtils.isNotEmpty(webHookListenerAOV0.getFilters())) {
            builder.put("FILTERS", webHookListenerAOV0.getFilters());
        }
        if (StringUtils.isNotEmpty(webHookListenerAOV0.getRegistrationMethod())) {
            builder.put("REGISTRATION_METHOD", webHookListenerAOV0.getRegistrationMethod());
        }
        if (StringUtils.isNotEmpty(webHookListenerAOV0.getParameters())) {
            builder.put("PARAMETERS", webHookListenerAOV0.getParameters());
        }
        if (StringUtils.isNotEmpty(webHookListenerAOV0.getDescription())) {
            builder.put("DESCRIPTION", webHookListenerAOV0.getDescription());
        }
        if (StringUtils.isNotEmpty(webHookListenerAOV0.getLastUpdatedUser())) {
            builder.put("LAST_UPDATED_USER", webHookListenerAOV0.getLastUpdatedUser());
        }
        if (webHookListenerAOV0.getLastUpdated() != null) {
            builder.put("LAST_UPDATED", webHookListenerAOV0.getLastUpdated().toInstant()
                    .atOffset(ZoneOffset.UTC)
                    .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        }
        builder.put("EXCLUDE_BODY", Boolean.toString(webHookListenerAOV0.isExcludeBody()));
        return builder.build();
    }

    private void migrate(ActiveObjects ao, WebHookListenerAOV0 webHookListenerAOV0) {
        Date now = new Date();
        ImmutableMap<String, Object> hook = ImmutableMap.<String, Object>builder()
                .put(AoWebhook.CREATED_COLUMN, now)
                .put(AoWebhook.UPDATED_COLUMN, now)
                .put(AoWebhook.URL_COLUMN, webHookListenerAOV0.getUrl())
                .put(AoWebhook.NAME_COLUMN, webHookListenerAOV0.getName())
                .put(AoWebhook.ACTIVE_COLUMN, webHookListenerAOV0.isEnabled())
                .put(AoWebhook.SCOPE_TYPE_COLUMN, WebhookScope.GLOBAL.getType())
                .build();

        AoWebhook aoWebhook = ao.create(AoWebhook.class, hook);

        createConfiguration(webHookListenerAOV0)
                .forEach((key, value) -> ao.create(AoWebhookConfigurationEntry.class,
                        ImmutableMap.of(AoWebhookConfigurationEntry.KEY_COLUMN, key,
                                AoWebhookConfigurationEntry.WEBHOOK_COLUMN_QUERY, aoWebhook.getID(),
                                AoWebhookConfigurationEntry.VALUE_COLUMN, value)));

        getEventsFor(webHookListenerAOV0)
                .forEach(event -> ao.create(AoWebhookEvent.class,
                        ImmutableMap.of(AoWebhookEvent.EVENT_ID_COLUMN, event.getId(),
                                AoWebhookEvent.WEBHOOK_COLUMN_QUERY, aoWebhook.getID())));
    }

    private Iterable<WebhookEvent> getEventsFor(WebHookListenerAOV0 webhook) {
        if (StringUtils.isEmpty(webhook.getEvents())) {
            return ImmutableList.of();
        }

        // events is a JSON array, we could bring
        ImmutableList.Builder<WebhookEvent> builder = ImmutableList.builder();
        try {
            JSONArray jsonArray = new JSONArray(webhook.getEvents());
            for (int i = 0; i < jsonArray.length(); i++) {
                String event = jsonArray.getString(i);
                builder.add(new MigrateWebhookEvent(event));
            }
        } catch (JSONException e) {
            log.warn("A webhook was unable to migrate the events for id:[{}]", webhook.getID());
        }
        return builder.build();
    }

    private static class MigrateWebhookEvent implements WebhookEvent {

        private final String id;

        MigrateWebhookEvent(String id) {
            this.id = id;
        }

        @Nonnull
        @Override
        public String getId() {
            return id;
        }

        @Nonnull
        @Override
        public String getI18nKey() {
            return id;
        }
    }
}
