package com.atlassian.webhooks.internal;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.Set;

@Component("validator")
public class DefaultValidator implements Validator {

    private final ServiceTracker<Object, Object> validatorTracker;

    @Autowired
    public DefaultValidator(BundleContext bundleContext) {
        validatorTracker = new ServiceTracker<>(bundleContext, "javax.validation.Validator", null);
        validatorTracker.open();
    }

    @PreDestroy
    public void destroy() {
        validatorTracker.close();
    }

    @Override
    public <T> T validate(T target) {
        Object validator = validatorTracker.getService();
        if (validator == null) {
            return target;
        }

        Set<ConstraintViolation<?>> validationErrors =
                new HashSet<>(((javax.validation.Validator) validator).validate(target));
        if (!validationErrors.isEmpty()) {
            throw new ConstraintViolationException(validationErrors);
        }
        return target;
    }
}
