package com.atlassian.webhooks.internal.dao.ao;

import net.java.ao.*;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

@Table(AoWebhook.TABLE_NAME)
public interface AoWebhook extends Entity {

    String ACTIVE_COLUMN = "ACTIVE";
    String CREATED_COLUMN = "CREATED";
    String NAME_COLUMN = "NAME";
    String SCOPE_ID_COLUMN = "SCOPE_ID";
    String SCOPE_TYPE_COLUMN = "SCOPE_TYPE";
    String TABLE_NAME = "WEBHOOK";
    String UPDATED_COLUMN = "UPDATED";
    String URL_COLUMN = "URL";

    @NotNull
    @OneToMany(reverse = "getWebhook")
    AoWebhookConfigurationEntry[] getConfiguration();

    @NotNull
    @OneToMany(reverse = "getWebhook")
    AoWebhookEvent[] getEvents();

    @Accessor(CREATED_COLUMN)
    @NotNull
    Date getCreatedDate();

    @Accessor(NAME_COLUMN)
    @NotNull
    @StringLength(255)
    String getName();

    @Accessor(SCOPE_ID_COLUMN)
    @StringLength(255)
    String getScopeId();

    @Accessor(SCOPE_TYPE_COLUMN)
    @NotNull
    @StringLength(255)
    String getScopeType();

    @Accessor(UPDATED_COLUMN)
    @NotNull
    Date getUpdatedDate();

    @Accessor(URL_COLUMN)
    @NotNull
    @StringLength(StringLength.UNLIMITED)
    String getUrl();

    @Accessor(ACTIVE_COLUMN)
    boolean isActive();

    @Mutator(ACTIVE_COLUMN)
    void setActive(boolean active);

    @Mutator(NAME_COLUMN)
    void setName(@Nonnull String name);

    @Mutator(SCOPE_ID_COLUMN)
    void setScopeId(@Nullable String id);

    @Mutator(SCOPE_TYPE_COLUMN)
    void setScopeType(@Nullable String type);

    @Mutator(UPDATED_COLUMN)
    void setUpdatedDate(@Nonnull Date date);

    @Mutator(URL_COLUMN)
    void setUrl(@Nonnull String url);
}
