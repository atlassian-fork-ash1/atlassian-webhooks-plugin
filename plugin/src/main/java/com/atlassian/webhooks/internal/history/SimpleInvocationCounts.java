package com.atlassian.webhooks.internal.history;

import com.atlassian.webhooks.history.InvocationCounts;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.time.Instant;

import static java.util.Objects.requireNonNull;

public class SimpleInvocationCounts implements InvocationCounts {

    private final Duration duration;
    private final int errors;
    private final int failures;
    private final int successes;

    public SimpleInvocationCounts(@Nonnull Duration duration, int errors, int failures, int successes) {
        this.duration = requireNonNull(duration, "duration");
        this.errors = errors;
        this.failures = failures;
        this.successes = successes;
    }

    @Override
    public int getErrors() {
        return errors;
    }

    @Override
    public int getFailures() {
        return failures;
    }

    @Override
    public int getSuccesses() {
        return successes;
    }

    @Nonnull
    @Override
    public Duration getWindowDuration() {
        return duration;
    }

    @Nonnull
    @Override
    public Instant getWindowStart() {
        return Instant.now().minus(duration);
    }
}
