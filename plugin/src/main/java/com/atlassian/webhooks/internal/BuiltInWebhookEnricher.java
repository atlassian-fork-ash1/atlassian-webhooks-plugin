package com.atlassian.webhooks.internal;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookRequestEnricher;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
@ExportAsService(WebhookRequestEnricher.class)
public class BuiltInWebhookEnricher implements WebhookRequestEnricher {

    @Override
    public void enrich(@Nonnull WebhookInvocation invocation) {
        invocation.getRequestBuilder()
                .header("X-Event-Key", invocation.getEvent().getId())
                .header("X-Request-Id", invocation.getId());
    }

    @Override
    public int getWeight() {
        return 0;
    }
}
