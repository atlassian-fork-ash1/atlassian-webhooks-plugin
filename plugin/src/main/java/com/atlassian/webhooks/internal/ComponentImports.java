package com.atlassian.webhooks.internal;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.httpclient.api.factory.HttpClientFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.scheduler.SchedulerService;
import org.springframework.stereotype.Component;

/**
 * Helper class to configure all OSGI imported services
 */
@Component("componentImports")
class ComponentImports {
    @ComponentImport("activeObjects") ActiveObjects activeObjects;
    @ComponentImport("applicationProperties") ApplicationProperties applicationProperties;
    @ComponentImport("eventPublisher") EventPublisher eventPublisher;
    @ComponentImport("httpClientFactory") HttpClientFactory httpClientFactory;
    @ComponentImport("moduleFactory") ModuleFactory moduleFactory;
    @ComponentImport("pluginAccessor") PluginAccessor pluginAccessor;
    @ComponentImport("schedulerService") SchedulerService schedulerService;
    @ComponentImport("transactionTemplate") TransactionTemplate transactionTemplate;
}
