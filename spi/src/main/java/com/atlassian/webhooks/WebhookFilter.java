package com.atlassian.webhooks;

import javax.annotation.Nonnull;

/**
 * A {@code WebhookFilter} is used to hook into the webhook process before the webhook has been sent, but after it has
 * been scheduled for firing. A filter can either be used to observe the webhooks that are being called, and lodge
 * callbacks against them, or to actually filter them out of the pipeline so that they will not be sent.
 *
 * @since 5.0
 */
public interface WebhookFilter {

    /**
     * Allows the invocation to be observed, and either blocked from further processing, or left alone.
     *
     * @param webhook the webhook that is raised for sending
     * @return true to allow the webhook to continue processing, false to block sending and further filter operations
     */
    boolean filter(@Nonnull WebhookInvocation webhook);

    /**
     * Gets the weight used to order the filters. Lower weights are processed first
     *
     * Values below 1000 are reserved for internal use.
     *
     * @return the weight
     */
    int getWeight();
}
