package com.atlassian.webhooks.api.events;

import com.atlassian.webhooks.api.register.listener.RegistrationMethod;

import java.util.Map;

@Deprecated
public class WebHookListenerDeletedEvent extends AbstractdWebHookListenerEvent {
    public WebHookListenerDeletedEvent(String name, String url, Iterable<String> events,
                                       Map<String, String> parameters, RegistrationMethod registrationMethod) {
        super(name, url, events, parameters, registrationMethod);
    }
}
