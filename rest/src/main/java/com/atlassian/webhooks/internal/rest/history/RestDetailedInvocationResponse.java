package com.atlassian.webhooks.internal.rest.history;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.history.DetailedInvocationResponse;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;

/**
 * The recorded {@link WebhookHttpResponse response} of a previous {@link WebhookInvocation webhook invocation}
 */
@JsonSerialize
public class RestDetailedInvocationResponse extends LinkedHashMap<String, Object> {

    @SuppressWarnings("unused") //Required by Jersey
    public RestDetailedInvocationResponse() {
    }

    public RestDetailedInvocationResponse(@Nonnull DetailedInvocationResponse response) {
        put("description", response.getDescription());
        put("headers", response.getHeaders());
        put("outcome", response.getOutcome());
        response.getBody().ifPresent(body -> put("body", body));
        put("statusCode", response.getStatusCode());
    }
}
