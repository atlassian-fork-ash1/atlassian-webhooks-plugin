package com.atlassian.webhooks.internal.rest.history;

import com.atlassian.webhooks.history.InvocationResult;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;

public class RestInvocationResult extends LinkedHashMap<String, Object> {

    public static final RestInvocationResult EXAMPLE = new RestInvocationResult(
            "200",
            "SUCCESS"
    );
    public static final RestInvocationResult EXAMPLE_FAILURE = new RestInvocationResult(
            "404",
            "FAILURE"
    );

    public static final RestInvocationResult EXAMPLE_ERROR = new RestInvocationResult(
            "Detailed error message",
            "ERROR"
    );

    private final String DESCRIPTION = "description";
    private final String OUTCOME = "outcome";

    @SuppressWarnings("unused") //Required by Jersey
    public RestInvocationResult() {
    }

    public RestInvocationResult(@Nonnull InvocationResult result) {
        put(DESCRIPTION, result.getDescription());
        put(OUTCOME, result.getOutcome());
    }

    public RestInvocationResult(String description, String outcome) {
        put(DESCRIPTION, description);
        put(OUTCOME, outcome);
    }
}
