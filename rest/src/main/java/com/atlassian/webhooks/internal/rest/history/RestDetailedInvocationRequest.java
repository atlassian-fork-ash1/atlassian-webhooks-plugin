package com.atlassian.webhooks.internal.rest.history;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.history.DetailedInvocationRequest;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;

/**
 * The recorded {@link WebhookHttpRequest request} of a previous {@link WebhookInvocation webhook invocation}
 */
@JsonSerialize
public class RestDetailedInvocationRequest extends LinkedHashMap<String, Object> {

    @SuppressWarnings("unused") //Required by Jersey
    public RestDetailedInvocationRequest() {
    }

    public RestDetailedInvocationRequest(@Nonnull DetailedInvocationRequest request) {
        put("url", request.getUrl());
        put("headers", request.getHeaders());
        put("method", request.getMethod().name());
        request.getBody().ifPresent(body -> put("body", body));
    }
}
